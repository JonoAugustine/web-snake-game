# Web Snake Game

Browser-based Snake Game with full controll over game settings.
Built with Parcel & NodeJS.

Live at https://jonoaugustine.gitlab.io/web-snake-game

![Menu](images/menu.png)
![playing](images/playing.png)

## TODO

In order of: High-Low Priority -> Completed

- Mobile controller
  - Touch Directional Pad
  - ~~Swipe controls~~
- Better pause menu
- Game-Over screen
  - Replay save
  - Score Save
- Keep score records
- Replay-saves
- More Documentation
- Better Score display
- Color customization
- Instructions (?)
- ~~Save settings~~
- ~~Changing game settings in the menu.~~
