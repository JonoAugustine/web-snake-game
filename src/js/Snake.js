import { grid, growthFactor, teleport } from "./settings";
import { overlap } from "./util";
import { getInputVector } from "./controller";

/**
 * Coordinates of each segmane of the snake body
 * @type {{x: int, y: int}[]}
 */
let body = [{ x: Math.floor(grid.x / 2), y: Math.floor(grid.y / 2) }];
let newSegments = 0;

body.head = () => body[0];

const reset = () => {
  body = [{ x: Math.floor(grid.x / 2), y: Math.floor(grid.y / 2) }];
  newSegments = 0;
  body.head = () => body[0];
};

/** Add growthFactor to newSegments */
const grow = () => (newSegments += growthFactor);

const getLength = () => body.length;

/**
 * @param {{x: int, y: int}} coord - The coordinates to check against
 *
 * @returns `true` if any segment of the snake body falls within
 * the given coordinates
 */
const overlaps = (coord) => {
  return body.some((seg) => overlap(seg, coord));
};

const addSegments = () => {
  for (let i = 0; i < newSegments; i++) {
    // Append tail to itself
    body[body.length] = { ...body[body.length - 1] };
  }

  newSegments = 0;
};

/**
 * @returns `true` if any part of the body is outside the grid.
 */
const outsideGrid = () => {
  return body.some(
    (seg) => seg.x < 1 || seg.x > grid.x || seg.y < 1 || seg.y > grid.y
  );
};

/**
 * @returns `true` if any part of the snake overlaps with another.
 */
const uroboros = () => {
  const headless = [...body];
  headless.shift();
  return headless.some((seg) => overlap(body.head(), seg));
};

/**
 * Update the position of each body segment
 */
const update = (inputVector) => {
  if (newSegments > 0) addSegments();

  for (let i = body.length - 2; i >= 0; i--) {
    // Take previous element and update to deep copy of current
    body[i + 1] = { ...body[i] };
  }

  // Update head
  body.head().x += inputVector.x;
  body.head().y += inputVector.y;

  if (!teleport) return;

  // Telleport to X start
  if (body.head().x > grid.x) {
    body.head().x = 1;
  }

  // Telleport to X end
  if (body.head().x < 1) {
    body.head().x = grid.x;
  }

  // Telleport to Y start
  if (body.head().y > grid.y) {
    body.head().y = 1;
  }

  // Telleport to Y end
  if (body.head().y < 1) {
    body.head().y = grid.y;
  }
};

/**
 * Draw each segment of the snake.
 * @param {HTMLElement} board - the game board
 */
const draw = (board) => {
  // Loop throuh each segment of the body
  body.forEach((seg, i) => {
    // Create a div and set it's position to the XY of the segment
    const segment = document.createElement("div");
    segment.classList.add("actor", "snake");

    if (i === 0) {
      segment.classList.add("head");
      const iv = getInputVector();
      segment.setAttribute("direction", iv.y === 0 ? "hor" : "vert");
    }

    segment.style.gridRowStart = seg.y;
    segment.style.gridColumnStart = seg.x;
    board.appendChild(segment);
  });
};

export {
  draw,
  update,
  grow,
  overlaps,
  outsideGrid,
  uroboros,
  reset,
  getLength,
};
