import { isRunning, togglePause, endGame } from "./engine";

let inputVector = { x: 0, y: 0 };
let lastInputVector = { x: 0, y: 0 };

const reset = () => {
  inputVector = { x: 0, y: 0 };
  lastInputVector = { x: 0, y: 0 };
};

const updateVector = (direction) => {
  switch (direction) {
    case "up":
      if (lastInputVector.y !== 0) break;
      inputVector = { x: 0, y: -1 };
      break;

    case "down":
      if (lastInputVector.y !== 0) break;
      inputVector = { x: 0, y: 1 };
      break;

    case "right":
      if (lastInputVector.x !== 0) break;
      inputVector = { x: 1, y: 0 };
      break;

    case "left":
      if (lastInputVector.x !== 0) break;
      inputVector = { x: -1, y: 0 };
      break;

    case "pause":
      togglePause();
      break;

    case "quit":
      endGame();
      break;
  }
};

// Listen for key events during runtime
window.addEventListener("keydown", (e) => {
  // Ignore controls if the engine is not running
  if (!isRunning()) return;

  switch (e.key) {
    case "ArrowUp":
    case "w":
      updateVector("up");
      break;

    case "ArrowDown":
    case "s":
      updateVector("down");
      break;

    case "ArrowRight":
    case "d":
      updateVector("right");
      break;

    case "ArrowLeft":
    case "a":
      updateVector("left");
      break;

    case "p":
      togglePause();
      break;

    case "l":
      endGame();
      break;
  }
});

// Swipe controls
window.addEventListener("swiped-up", () => updateVector("up"));
window.addEventListener("swiped-down", () => updateVector("down"));
window.addEventListener("swiped-left", () => updateVector("left"));
window.addEventListener("swiped-right", () => updateVector("right"));

const getInputVector = () => {
  lastInputVector = inputVector;
  return inputVector;
};

export { getInputVector, reset };
