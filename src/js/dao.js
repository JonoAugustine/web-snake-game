// LocalStorage "DAO"
import * as Settings from "./settings";

const SETTINGS = "SETTINGS";
const SCORES = "SCORES";
const REPLAYS = "REPLAYS";

class ScoreRecord {
  constructor(name, size, grid, date) {
    this.name = name;
    this.size = size;
    this.grid = grid;
    this.date = date;
  }

  json() {
    return JSON.stringify(this);
  }
}

String.prototype.asScoreRecord = function () {
  return new ScoreRecord(...JSON.parse(this));
};

const save = (key, value) => (localStorage[key] = JSON.stringify(value));

const load = (key) => JSON.parse(localStorage[key] || "{}");

const loadScores = () => {
  return load(SCORES) || [];
};

const saveScore = ({ name, size }) => {
  let scores = loadScores();
  if (Object.keys(scores).length === 0) scores = [];
  try {
    save(SCORES, [
      ...scores,
      new ScoreRecord(name, size, Settings.grid, Date.now()),
    ]);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

// TODO const saveReplay;
// TODO const loadReplays;

const saveSettings = () => {
  const svals = {};
  for (let key in Settings) {
    if (typeof Settings[key] !== "function") {
      svals[key] = Settings[key];
    }
  }
  save(SETTINGS, svals);
};

const loadSettings = () => {
  const loaded = load(SETTINGS);
  if (Object.keys(loaded).length > 0) {
    for (let key in loaded) {
      Settings[key] = loaded[key];
    }
  }
};

export { saveSettings, loadSettings, loadScores, saveScore };
