import { speed, grid, teleport } from "./settings";
import * as Controller from "./controller";
import * as Snake from "./Snake";
import * as Food from "./food";
import * as Menu from "./menu";
import * as Dao from "./dao";

const BOARD = document.getElementById("board");

let lastRenderTime;
let running = false;
let paused = false;
let gameOver = false;
let score = 1;
const incrementScore = (by = 1) => (score += by);

/** @returns `true` if the engine is running */
const isRunning = () => running;
const isPaused = () => paused;

const main = (time) => {
  if (gameOver || !running) {
    return endGame();
  }

  // Await for window ready to render
  window.requestAnimationFrame(main);

  if (paused) return;

  /** Seconds since last render */
  const renderDelay = (time - lastRenderTime) / 1000;

  // Ignore renders that are too fast
  if (renderDelay < 1 / speed) return;

  // Update last render time to time param
  lastRenderTime = time;

  update();
  draw();
};

const start = () => {
  // Set grid
  BOARD.style.gridTemplateColumns = `repeat(${grid.x}, 1fr)`;
  BOARD.style.gridTemplateRows = `repeat(${grid.y}, 1fr)`;

  Menu.close();

  gameOver = false;

  // Begin engine loop
  window.requestAnimationFrame(main);
  running = true;
};

/**
 * Updates the game state
 */
const update = () => {
  Snake.update(Controller.getInputVector());
  Food.update();
  checkDeath();
};

/**
 * Renders the game board based on current state
 */
const draw = () => {
  BOARD.innerHTML = "";
  Menu.updateScores();
  Snake.draw(BOARD);
  Food.draw(BOARD);
};

const togglePause = () => {
  paused = !paused;
  if (paused) Menu.open();
  else Menu.close();
};

const checkDeath = () => {
  if (!teleport) {
    gameOver = Snake.outsideGrid();
  }

  gameOver = gameOver || Snake.uroboros();
};

const endGame = () => {
  running = false;
  paused = false;
  gameOver = true;
  score = 0;
  Snake.reset();
  Food.reset();
  Controller.reset();
  BOARD.innerHTML = "";
  Menu.open();
};

export {
  main,
  start,
  score,
  isRunning,
  isPaused,
  togglePause,
  incrementScore,
  endGame,
  gameOver,
};
