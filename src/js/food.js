import { randomCoord } from "./util";
import { grid, spawnBorder } from "./settings";
import * as Snake from "./Snake";
import { incrementScore } from "./engine";

/**
 * Coordinates of the food actor
 * @type {{x: int, y: int}}
 */
let food = null;

const randomPos = () => {
  let nextPos = null;

  while (!nextPos || Snake.overlaps(nextPos)) {
    nextPos = {
      x: randomCoord(grid.x - spawnBorder, spawnBorder || 1), // Math.max(border, 1)
      y: randomCoord(grid.y - spawnBorder, spawnBorder || 1),
    };
  }

  return nextPos;
};

const reset = () => {
  food = null;
};

/**
 *
 */
const update = () => {
  if (food === null) food = randomPos();

  if (Snake.overlaps(food)) {
    Snake.grow();
    food = randomPos();
    incrementScore();
  }
};

/**
 * Draw the food element
 * @param {HTMLElement} board - the game board
 */
const draw = (board) => {
  // Create a div and set it's position to the XY of the actor
  const element = document.createElement("div");
  element.classList.add("actor", "food");
  element.style.gridRowStart = food.y;
  element.style.gridColumnStart = food.x;
  board.appendChild(element);
};

export { draw, update, reset };
