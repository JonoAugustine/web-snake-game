import "./swiped-events.min.js";
import * as Engine from "./engine";
import * as Menu from "./menu";
import * as Dao from "./dao";

Dao.loadSettings();
Menu.open();
