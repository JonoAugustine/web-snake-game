import { start, score, isPaused, gameOver } from "./engine";
import {
  speed,
  grid,
  growthFactor,
  teleport,
  setGrid,
  setGrowthFactor,
  setTeleport,
  setSpeed,
  spawnBorder,
  setSpawnBorder,
} from "./settings";
import { saveSettings, saveScore } from "./dao";
import { getLength } from "./Snake";

const menu = document.getElementById("menu");
const settingsTab = document.querySelector(".settings-tab");
const gameoverTab = document.querySelector(".gameover-tab");

document.getElementById("start-game").onclick = () => start();

const speedInput = document.getElementById("speed");
speedInput.addEventListener("change", (e) => {
  setSpeed(e.target.value);
  speedInput.value = speed;
  saveSettings();
});

const gridXInput = document.getElementById("grid-x");

gridXInput.addEventListener("change", (e) => {
  setGrid(parseInt(e.target.value));
  saveSettings();
});

const gridYInput = document.getElementById("grid-y");
gridYInput.addEventListener("change", (e) => {
  setGrid(null, parseInt(e.target.value));
  saveSettings();
});

const gFactorInput = document.getElementById("grow-factor");
gFactorInput.addEventListener("change", ({ target: { value } }) => {
  setGrowthFactor(parseInt(value));
  saveSettings();
});

const teleportInput = document.getElementById("teleport");
teleportInput.addEventListener("change", (e) => {
  setTeleport(e.target.checked);
  saveSettings();
});

const spawnBorderInput = document.getElementById("spawn-border");
spawnBorderInput.addEventListener("change", ({ target: { value } }) => {
  const max = Math.min(grid.y, grid.x) / 2 - 1;
  spawnBorderInput.setAttribute("max", max);
  if (value < max) setSpawnBorder(parseInt(value));
  spawnBorderInput.value = spawnBorder;
  saveSettings();
});

const postGameForm = document.getElementById("postGame");
postGameForm.onsubmit = (e) => {
  e.preventDefault();

  const saveScoreChecked = e.target[0].checked;
  const saveReplayChecked = e.target[1].checked;
  const name = e.target[2].value;

  if ((saveReplayChecked || saveScoreChecked) && name.length < 2) {
    return;
  }

  if (saveScoreChecked) saveScore({ name, size: getLength() });

  if (saveReplayChecked) {
    // TODO call replay save
  }

  settingsTab.style.display = "inline";
  gameoverTab.style.display = "none";
};

const scoresDisplays = document.querySelectorAll(".score-display");

const updateScores = () => {
  scoresDisplays.forEach(
    (sd) =>
      (sd.textContent =
        `${Math.floor(score)} / ${grid.x * grid.y} ` +
        `(${Math.floor((score / (grid.y * grid.x)) * 100)}%)`)
  );
};

const open = () => {
  speedInput.value = speed;
  gridXInput.value = grid.x;
  gridYInput.value = grid.y;
  gFactorInput.value = growthFactor;
  teleportInput.checked = teleport;
  spawnBorderInput.value = spawnBorder;
  menu.style.display = "inline";

  if (gameOver) {
    settingsTab.style.display = "none";
    gameoverTab.style.display = "inline";
  }
};

const close = () => (menu.style.display = "none");

export { open, close, updateScores };
