/** Speed of the game render, i.e. the speed of the snake, in updates per second */
let speed = 5;

const setSpeed = (ns) => (speed = Math.min(35, ns));

/** The dimentions of the board grid */
let grid = { x: 21, y: 21 };

const setGrid = (x, y) => (grid = { x: x || grid.x, y: y || grid.y });

/**
 * The number of segments added each time
 * the snake "eats" a food actor
 */
let growthFactor = 1;

const setGrowthFactor = (ngf) => (growthFactor = ngf);

/** Whether the snake should teleport at the edge of the grid or die */
let teleport = false;

const setTeleport = (bool) => (teleport = bool);

/** The number of grid spaces around the edge where the food cannot spawn */
let spawnBorder = 0;

const setSpawnBorder = (border) => (spawnBorder = border);

export {
  speed,
  grid,
  growthFactor,
  teleport,
  spawnBorder,
  setSpawnBorder,
  setGrid,
  setGrowthFactor,
  setSpeed,
  setTeleport,
};
