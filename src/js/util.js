export const randomCoord = (max, min = 1) => {
  return min + Math.floor(Math.random() * (max - min + 1));
};

/**
 * @param {{x: int, y: int}} a - The first coordinate pair
 * @param {{x: int, y: int}} b - The second coordinate pair
 *
 * @returns `true` if the given coord pairs are equal
 */
export const overlap = (a, b) => {
  return a.x === b.x && a.y === b.y;
};
